# FG Tournament Connection Verifier

This project focuses on the development of a network application capable of calculating expected delay of Delay Netcode based Fighting Games

The repo will follow the development accordingly with changelogs and updates, this is the planned feature list:

* Server application for Tournament Organizers to use as node for collecting data, which it then uses to calculate delay using a formula
* Lightweight client application that receives instructions from Server and send back data



**TO DO LIST**
* [X]  **Server**
    * [X] Basics
    * [X] Multi-thread support for handling multiple connections
    * [X] Gather clients, assign ID to each and map to their IP address
    * [X] Receive data
    * [X] Process data to remove garbage in buffer
    * [X] Send IP list back to clients
    * [X] Receive ping results from clients
    * [X] Process ping results in Delay Calculation (Currently unsure if correct)
    * [X] Print results

----

* [X]  **Client**
    * [X] Basics
    * [X] Receive data
    * [X] Expect and process IP list from server
    * [X] Ping clients from IP list, ~~excluding own~~ (now done serverside)
    * [X] Collect results and send back to server

---

* [ ]  **Major bug-fixing and reworking**
    * [ ] TBD
