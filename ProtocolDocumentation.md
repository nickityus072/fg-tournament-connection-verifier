# The Table below explains the procedure done through RAW protocol for Server/Client interaction

|                                    Server                                    |                                                Client                                               |
|:----------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------------:|
| CMD_PINGMODE, Tell client to start receiving ips                             |                                                                                                     |
|                                                                              | CMD_PINGMODESTARTED, Tell server thread to start sending and then receive results on a client basis |
| CMD_LISTSTARTED, Tell client that we are about to send vector                |                                                                                                     |
|                                                                              | CMD_OK, Tell server that we confirm and ready to receive                                            |
| Send vector, wait for CMD_RECEIVED                                           |                                                                                                     |
|                                                                              | CMD_RECEIVED, Tell server that we received vector                                                   |
| CMD_END, Tell client that we have no more to send, client will start pinging |                                                                                                     |
|                                                                              | CMD_PINGRESULTS, Tell server that we are sending PingResults vector                                 |
| CMD_OK, Tell client that we confirm and ready to receive                     |                                                                                                     |
|                                                                              | Send vector, wait for CMD_RECEIVED                                                                  |
| CMD_RECEIVED, Tell client that we received vector                            |                                                                                                     |
| Process Results Server-side                                                  |                                                                                                     |
