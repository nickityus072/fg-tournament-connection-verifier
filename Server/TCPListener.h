#pragma once
#pragma comment(lib, "ws2_32.lib")
#include <iostream>
#include <string>
#include <vector>
#include <winsock.h>
#include "TCPListener.h"
using namespace std::chrono_literals;

class ClientInfo
{
public:
	SOCKET socket;
	std::string ip;
	USHORT port;
	std::string alias;
	int id;

	ClientInfo(SOCKET a, std::string b, USHORT c, std::string d, int e)
	{
		socket = a;
		ip = b;
		port = c;
		alias = d;
		id = e;
	}
};

class PingInfo
{
public:
	std::string ip;
	unsigned long rtt;
	std::string who;
	std::string alias;
	std::string whoalias;

	PingInfo(std::string a, std::string b, std::string c, std::string d, std::string e)
	{
		ip = a;
		rtt = strtoul(b.c_str(), NULL, 0);
		who = c;
		alias = d;
		whoalias = e;
	}
};

class TCPListener
{
public:
	TCPListener(const TCPListener&) = delete;
	void operator=(const TCPListener&) = delete;
	TCPListener() {};
public:
	u_long sMode = 0;
	int newID = 0;
	SOCKET hostingSocket{};
	SOCKET clientSocket{};
	SOCKADDR_IN serverdef{};
	SOCKADDR_IN clientdef{};
	int clientdefsize = sizeof(clientdef);
	std::string finalMessage;
	std::vector<ClientInfo> Clients;
	std::vector<PingInfo> results;

	// WINSOCK INIT
	void init()
	{
		WSAData wsaData;
		WORD sockVersion = MAKEWORD(2, 2);
		if (WSAStartup(sockVersion, &wsaData) != 0) {
			std::cout << "WSAStartup Error!"; ExitProcess(EXIT_FAILURE);
		}

	}

	// CREATE HOST SOCKET
	void createHostSocket() {
		hostingSocket = socket(AF_INET, SOCK_STREAM, 0);
		if (hostingSocket < 0) {
			std::cout << "Hosting Socket Error!"; ExitProcess(EXIT_FAILURE);
		}
	}

	// DEFINE SERVER PROTOCOL SETTINGS
	void defineServer(int port)
	{
		ZeroMemory(&serverdef, sizeof(serverdef));
		serverdef.sin_family = AF_INET;
		serverdef.sin_port = htons(port);
		serverdef.sin_addr.S_un.S_addr = INADDR_ANY;
	}

	// BIND SERVER ON PORT
	void bindServer() {
		if (bind(hostingSocket, (sockaddr*)&serverdef, sizeof(serverdef)) != 0)
		{
			std::cout << "Bind Error! " << WSAGetLastError() << "\n\n"; ExitProcess(EXIT_FAILURE);
		}
	}

	// START LISTENING
	void startListen()
	{
		listen(hostingSocket, SOMAXCONN);
	}

	// ACCEPT CLIENTS
	void acceptClient()
	{
		clientSocket = accept(hostingSocket, (sockaddr*)&clientdef, &clientdefsize);

		ioctlsocket(clientSocket, FIONBIO, (u_long*)&sMode);
		ClientInfo client(clientSocket, inet_ntoa(clientdef.sin_addr), ntohs(clientdef.sin_port), "", newID);
		Clients.push_back(client);
		newID++;
	}

	// SEND MESSAGE ON CONNECT
	void sendWelcome(ClientInfo& Client)
	{
		sendData(Client, "Connected!\r\n");
	}

	// PRINT DISCONNECT MESSAGE
	void printDisconnect(ClientInfo& Client)
	{
		std::cout << "\n\n(" << Client.ip << " [ClientID: " << Client.id << ']' << " Disconnected!" << ")\n\n";
	}

	// REMOVE DISCONNECTED CLIENT FROM LIST
	void cleanDisconnect(ClientInfo& Client)
	{
		bool vecCheck = true;
		auto it = results.begin();
		while (vecCheck)
		{
			if (it->who == Client.ip)
			{
				it = results.erase(it);
				it = results.begin();
			}
			else if (it != results.end()) ++it;
			if (it == results.end())
			{
				vecCheck = false;
			}
		}

		vecCheck = true;
		auto it2 = Clients.begin();
		while (vecCheck)
		{
			if (it2->id == Client.id)
			{
				it2 = Clients.erase(it2);
				it2 = Clients.begin();
			}
			else if (it2 != Clients.end()) ++it2;
			if (it2 == Clients.end())
			{
				vecCheck = false;
			}
		}
	}

	// RECEIVE DATA FROM CLIENT
	std::string receiveData(ClientInfo& Client)
	{
		char buf[128] = {};
		int connectStatus = 1;
		int bytefullcount = 0;
		int recvRetry = 0;
		while (true)
		{
			int byteCount = 0;
			byteCount = recv(Client.socket, buf, 128, 0);

			// ERROR HANDLING
			if (byteCount == SOCKET_ERROR)
			{
				if (WSAGetLastError() == 10054) { printDisconnect(Client); cleanDisconnect(Client); connectStatus = 0; break; }
				if (WSAGetLastError() == 10035)
				{
					if (recvRetry <= 5)
					{
						recvRetry++;
						continue;
					}
					else { return "EMPTY"; }
				}

				else { std::cout << "Socket Error! " << WSAGetLastError(); }
				connectStatus = 0;
				break;
			}
			if (byteCount == 0) { printDisconnect(Client); cleanDisconnect(Client); connectStatus = 0; break; }

			// NEWLINE ONLY MESSAGES IGNORED
			if (buf[0] == '\r') { bytefullcount = 0; continue; }
			if (buf[0] == '\n') { bytefullcount = 0; continue; }
			else bytefullcount += byteCount;

			// CLEANUP NEWLINES
			int e = byteCount;
			for (int n = 0; n <= e; n++)
			{
				if (buf[n] == '\n') { buf[n] = 0; n = 0; }
				if (buf[n] == '\r') { buf[n] = 0; n = 0; }
			}
			//////////////////


			std::string finalMessage = buf;
			ZeroMemory(buf, sizeof(buf));

			if (bytefullcount > 0)
			{
				return finalMessage;
			}
		}

		if (connectStatus == 0)
		{
			finalMessage = "CONN_END";
			return finalMessage;
		}
		return "NULL";
	}

	// ASK CLIENT FOR ALIAS INPUT
	ClientInfo askAlias(ClientInfo& Client)
	{
		sendData(Client, "Please input your nickname: ");

		std::string alias = receiveData(Client);
		for (auto it = Clients.begin(); it != Clients.end(); ++it)
		{
			int pos = std::distance(Clients.begin(), it);
			if (it->id == Client.id)
			{
				Clients[pos].alias = alias;
				Client = Clients[pos];
			}
		}
		
		printAssign(Client);

		return Client;
	}

	// SEND DATA TO CLIENT
	void sendData(ClientInfo& Client, std::string message)
	{
		char sendbuf[512];
		ZeroMemory(sendbuf, sizeof(sendbuf));
		strcpy_s(sendbuf, sizeof(sendbuf), message.c_str());
		send(Client.socket, sendbuf, strlen(sendbuf), 0);
	}

	// PRINT ALIAS ASSIGNATION
	void printAssign(ClientInfo& Client)
	{
		std::string finalMessage = "You are now " + Client.alias + '\r' + '\n';
		std::cout << Client.ip << " [ClientID: " << Client.id << ']' << " is now " << Client.alias << "\n";

		sendData(Client, finalMessage);
	}

	// PRINT CONNECTIONS
	void printConnected()
	{
		auto Client = Clients.back();
		std::cout << "\n\n(" << Client.ip << " connected on port " << Client.port << '!' << ")\n\n";
	}

	// PRINT RAW DATA
	void printRaw(ClientInfo& Client, std::string message)
	{
		std::cout << Client.alias << " (" << Client.ip << ") " << "sent: " << message << '\n';
	}

	// SEND "YOU SENT: " TO CLIENT
	void resendToClient(ClientInfo& Client, std::string message)
	{
		std::string finalMessage = "You sent: " + message + '\n';
		sendData(Client, finalMessage);
	}

	// ACCEPT CLIENTS
	void getClient()
	{
		acceptClient();
		printConnected();
	}

	// INIT AND START LISTEN
	void start(int port)
	{
		init();
		createHostSocket();
		defineServer(port);
		bindServer();
		startListen();
	}
};