#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "ws2_32.lib")
#include <iostream>
#include <string>
#include <winsock.h>
#include <thread>
#include <string>
#include <Windows.h>
#include <conio.h>
#include <math.h>
#include <mutex>
#include "TCPListener.h"
using namespace std::chrono_literals;
using namespace std::chrono;
using namespace std::this_thread;

TCPListener s;
std::mutex m;

void enableStdinEcho(bool b) {
	HANDLE hStdin = ::GetStdHandle(STD_INPUT_HANDLE);
	DWORD mode = 0;
	::GetConsoleMode(hStdin, &mode);
	if (b) {
		mode |= ENABLE_ECHO_INPUT;
	}
	else {
		mode &= ~ENABLE_ECHO_INPUT;
	}
	::SetConsoleMode(hStdin, mode);
}
int to_delay(unsigned long ping)
{
	int delay;
	int constant = 30;
	delay = ceil((ping + constant) / (2 * 16.667));
	return delay;
}
void listClients()
{
	std::cout << "\n\n\t" << "CLIENT LIST" << "\n";
	for (auto Client : s.Clients)
	{
		std::cout <<
			"\nClient ID: \t\t" << Client.id <<
			"\nClient IP: \t\t" << Client.ip <<
			"\nClient Alias: \t\t" << Client.alias <<
			"\n\n";
	}
}
std::string listResults()
{
	std::string resultlist;

	std::cout << "\n\n\t" << "RESULTS" << "\n";
	for (auto Result : s.results)
	{
		std::cout <<
			"\nFrom: \t\t\t" << Result.whoalias <<
			"\nTarget: \t\t" << Result.alias <<
			"\nDelay: \t\t\t" << to_delay(Result.rtt) <<
			"\nPing: \t\t\t" << Result.rtt << "ms" <<
			"\n";
	}

	for (auto Result : s.results)
	{
		resultlist +=
			"\nFrom: \t\t\t" + Result.whoalias +
			"\nTarget: \t\t" + Result.alias +
			"\nDelay: \t\t\t" + std::to_string(to_delay(Result.rtt)) +
			"\nPing: \t\t\t" + std::to_string(Result.rtt) + "ms" +
			"\n";
	}
	return resultlist;
}
void processResults()
{
	// CHECK FOR SELF PINGS
	for (auto it = s.results.begin(); it != s.results.end(); ++it)
	{
		if (it->ip == it->who)
		{
			it = s.results.erase(it);
		}
	}

	// ASSIGN CLIENT ALIAS TO EACH SIDE
	for (auto it = s.results.begin(); it != s.results.end(); ++it)
	{
		int pos = std::distance(s.results.begin(), it);
		if (it->alias == "Unknown")
		{
			for (auto Client : s.Clients)
			{
				if (Client.ip == it->ip)
				{
					s.results[pos].alias = Client.alias;
				}
			}
		}
		if (it->whoalias == "Unknown")
		{
			for (auto Client : s.Clients)
			{
				if (Client.ip == it->who)
				{
					s.results[pos].whoalias = Client.alias;
				}
			}
		}
	}
}
void startProcedure()
{
	char confirm;
	std::cout << "\nPress Y to proceed / Press N to cancel.\n";
	confirm = toupper(_getch());

	if (confirm == 'Y')
	{
		std::cout << "Confirmed!\n\n";

		for (auto Client : s.Clients)
		{
			s.sendData(Client, "CMD_PINGMODE");
		}
	}
	if (confirm == 'N')
	{
		std::cout << "Cancelled!\n";
	}
}
void InterruptHandler()
{
	while (true)
	{
		char c = toupper(_getch());
		if (c == 'T')
		{
			listClients();
		}
		if (c == 'Y')
		{
			startProcedure();
		}
	}
}
void deserialize(std::string serializedResults, ClientInfo& Client)
{
	std::string ip;
	std::string rtt;
	char toRead[512];
	ZeroMemory(toRead, 512);
	strcpy(toRead, serializedResults.c_str());

	std::string what = "ip";
	for (int i = 0; i <= sizeof(toRead); i++)
	{
		if (what == "ip" && toRead[i] != ',' && toRead[i] != ';' && toRead[i] != '\0')
		{
			ip += toRead[i];
		}
		else if (what == "ip" && toRead[i] == ',' && toRead[i] != ';' && toRead[i] != '\0')
		{
			what = "rtt";
		}

		if (what == "rtt" && toRead[i] != ',' && toRead[i] != ';' && toRead[i] != '\0')
		{
			rtt += toRead[i];
		}

		if (toRead[i] == ';')
		{

			PingInfo result(ip, rtt, Client.ip, "Unknown", "Unknown");
			ip = "";
			rtt = "";
			s.results.push_back(result);
			what = "ip";
		}
	}
	ZeroMemory(toRead, 512);
}
void handleProcedure(ClientInfo& Client)
{
	s.sendData(Client, "CMD_LISTSTARTED");

	std::string serializedIPs;
	bool notOK = true;

	for (auto Client : s.Clients)
	{
		serializedIPs += Client.ip + ',';
	}
	//serializedIPs = "194.150.168.168,210.134.0.130,"; (German DNS and Japanese DNS for testing)

	while (notOK)
	{
		if (s.receiveData(Client) == "CMD_OK")
		{
			std::cout << Client.alias << ": Step 1/3...\n";
			s.sendData(Client, serializedIPs);
			notOK = false;
		}
		sleep_for(1s);
	}
	notOK = true;

	while (notOK)
	{
		if (s.receiveData(Client) == "CMD_RECEIVED")
		{
			std::cout << Client.alias << ": Step 2/3...\n";
			s.sendData(Client, "CMD_END");
			notOK = false;
		}
		sleep_for(1s);
	}
	notOK = true;

	while (notOK)
	{
		if (s.receiveData(Client) == "CMD_PINGRESULTS")
		{
			std::cout << Client.alias << ": Step 3/3\n";
			std::cout << Client.alias << ": Waiting to receive data from client...\n";
			s.sendData(Client, "CMD_OK");
			m.lock();
			std::string serializedResults = s.receiveData(Client);
			deserialize(serializedResults, Client);
			m.unlock();
			s.sendData(Client, "CMD_RECEIVED");
			std::cout << Client.alias << ": Done!\n";
			notOK = false;
		}
		sleep_for(1s);
	}
	notOK = true;

	m.lock();
	processResults();
	m.unlock();
	m.lock();
	std::string toSend = listResults();
	s.sendData(Client, toSend);
	m.unlock();

}
void ClientReceiveThread(ClientInfo Client)
{
	// SEND "CONNECTED!" MESSAGE TO CLIENT
	s.sendWelcome(Client);

	// ASK CLIENT FOR NICKNAME AND UPDATE CLIENT
	Client = s.askAlias(Client);

	// RECEIVE AND PRINT LOOP
	while (true)
	{
		std::string message = s.receiveData(Client);

		if (message == "CONN_END" || message == "NULL")
		{
			break;
		}

		if (message == "CMD_PINGMODESTARTED")
		{
			std::cout << "\nCommencing Ping Mode...\n";
			handleProcedure(Client);
		}
		else
		// PRINT WHAT IS RECEIVED TO CONSOLE
		s.printRaw(Client, message);
		// RESEND TO CLIENT UNDER "YOU SENT: "
//s.resendToClient(Client, message);
	}
	closesocket(Client.socket);
}
void ServerThread(int port)
{
	while (true)
	{
		// ACCEPT CLIENTS
		s.getClient();

		// GIVE THE CLIENT TO A THREAD AND DETACH
		auto Client = s.Clients.back();
		std::thread ClientReceiveThreadt(ClientReceiveThread, Client);
		ClientReceiveThreadt.detach();
	}
}
int main()
{
	// DECLARATIONS
	std::string portinput;
	int port;

	// PORT PROMPT
	std::cout << "Input Port: ";
	std::getline(std::cin, portinput);
	port = stoi(portinput);


	// DISABLE INPUTS PRINTING OUT ON CONSOLE
	enableStdinEcho(false);

	// START SERVER AND LISTEN ON PORT
	s.start(port);

	std::cout << "Waiting for connections... " << "\n\n";
	std::cout << "Press T to display connected clients." << '\n';
	std::cout << "Press Y at any moment to begin procedure." << '\n';

	// MOVE SERVER ON A DIFFERENT THREAD
	std::thread ServerThreadt(ServerThread, port);


	while (true)
	{
		// START INTERRUPT HANDLER FOR INTERACTION
		InterruptHandler();
	}

	ServerThreadt.join();
	return 0;
}


/*
													--- PROTOCOL ---
SERVER																CLIENT
CMD_PINGMODE, Tell client to start receiving ips
																	CMD_PINGMODESTARTED, Tell server thread to start sending and then receive results on a client basis
CMD_LISTSTARTED, Tell client that we are about to send vector
																	CMD_OK, Tell server that we confirm and ready to receive
Send vector, wait for CMD_RECEIVED
																	CMD_RECEIVED, Tell server that we received vector
CMD_END, Tell client that we have no more to send,
client will start pinging

																	CMD_PINGRESULTS, Tell server that we are sending PingResults vector
CMD_OK, Tell client that we confirm and ready to receive
																	Send vector, wait for CMD_RECEIVED
CMD_RECEIVED, Tell client that we received vector

Start processing results server-side
*/