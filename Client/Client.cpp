#define _CRT_SECURE_NO_WARNINGS


#include <iostream>
#include <string>
#include <thread>
#include <map>
#include "TCPClient.h"
using namespace std::chrono_literals;
using namespace std::chrono;
using namespace std::this_thread;

void deserialize(TCPClient& c, std::string serializedIPs, ServerInfo& Server)
{
	std::string ip;
	int listLength = serializedIPs.length() + 1;
	char* toRead = new char[listLength];
	strcpy(toRead, serializedIPs.c_str());

	for (int i = 0; i <= listLength; i++)
	{
		if (toRead[i] != ',')
		{
			ip += toRead[i];
		}
		else if (toRead[i] == ',')
		{
			c.ips.push_back(ip);
			ip = "";
		}
	}
}

int handleProcedure(TCPClient c, ServerInfo& Server)
{
	std::string message;
	c.sendData(Server, "CMD_PINGMODESTARTED");
	std::string serializedIPs;
	std::string serializedResults;
	bool notOK = true;

	while (notOK)
	{
		message = c.receiveData(Server);
		if (message == "CMD_LISTSTARTED")
		{
			std::cout << "Step 1/3...\n";
			c.sendData(Server, "CMD_OK");
			serializedIPs = c.receiveData(Server);
			c.sendData(Server, "CMD_RECEIVED");
		}
		if (message == "CONN_END" || message == "NULL")
		{
			return -1;
		}
		message = c.receiveData(Server);
		if (message == "CMD_END")
		{
			std::cout << "Step 2/3...\n";
			notOK = false;
		}
		if (message == "CONN_END" || message == "NULL")
		{
			return -1;
		}
		sleep_for(1s);
	}
	notOK = true;

	deserialize(c, serializedIPs, Server);
	c.ping();

	c.sendData(Server, "CMD_PINGRESULTS");

	while (notOK)
	{
		message = c.receiveData(Server);
		if (message == "CMD_OK")
		{
			std::cout << "Step 3/3\nSending Data to Server...\n";
			for (auto result : c.results)
			{
				serializedResults += result.ip + ',' + std::to_string(result.rtt) + ';';
			}
			c.sendData(Server, serializedResults);
		}
		if (message == "CONN_END" || message == "NULL")
		{
			return -1;
		}

		message = c.receiveData(Server);
		if (message == "CMD_RECEIVED")
		{
			std::cout << "Done!\n";
			return 0;
		}
		if (message == "CONN_END" || message == "NULL")
		{
			return -1;
		}
	}
}

void ReceiveThread(TCPClient c, ServerInfo Server)
{
	while (true) {
		std::string message = c.receiveData(Server);
		
		if (message == "CONN_END" || message == "NULL")
		{
			break;
		}
		if (message == "CMD_PINGMODE")
		{
			std::cout << "\n\nCommencing Ping Mode...\n";
			int err = handleProcedure(c, Server);
			if (err == -1)
			{
				break;
			}
		}
		else

		c.printData(message);
	}
	closesocket(Server.socket);
	ExitProcess(EXIT_SUCCESS);
}

int main()
{
	// DECLARATIONS
	TCPClient c;
	std::string ip;
	std::string portinput;
	int port;


	//// IP AND PORT INPUT
	std::cout << "Input IP: ";
	std::getline(std::cin, ip);
	
	std::cout << "Input Port: ";
	std::getline(std::cin, portinput);
	port = stoi(portinput);
	std::cout << '\n';

	// INIT AND CONNECT TO SERVER
	ServerInfo Server(NULL, ip, port);
	Server = c.start(Server);

	// RECEIVE DATA FROM SERVER ON ANOTHER THREAD
	std::thread ReceiveThreadt(ReceiveThread, c, Server);
	ReceiveThreadt.detach();
		
	while (true) {
		c.sendInput(Server);
	}
}
