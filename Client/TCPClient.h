#pragma once

#pragma comment(lib, "WS2_32.lib")
#pragma comment(lib, "iphlpapi.lib")
#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <winsock.h>
#include <Windows.h>
#include <iphlpapi.h>
#include <IcmpAPI.h>
using namespace std::chrono_literals;
using namespace std::chrono;
using namespace std::this_thread;

class ServerInfo
{
public:
	SOCKET socket;
	std::string ip;
	USHORT port;

	ServerInfo(SOCKET a, std::string b, USHORT c)
	{
		socket = a;
		ip = b;
		port = c;
	}

	void setSocket(SOCKET a)
	{
		socket = a;
	}
};

class PingInfo
{
public:
	std::string ip;
	unsigned long rtt;

	PingInfo(std::string a, unsigned long b)
	{
		ip = a;
		rtt = b;
	}
};

class TCPClient 
{
public:
	SOCKET serverSocket{};
	SOCKADDR_IN serverdef{};
	std::vector<std::string> ips;
	std::vector<PingInfo> results;

	// WINSOCK INIT
	void init()
	{
		WSAData wsaData;
		WORD sockVersion = MAKEWORD(2, 2);
		if (WSAStartup(sockVersion, &wsaData) != 0) {
			std::cout << "WSAStartup Error!"; ExitProcess(EXIT_FAILURE);
		}

	}
	// CREATE CLIENT SOCKET
	void createClientSocket()
	{
		serverSocket = socket(AF_INET, SOCK_STREAM, 0);
		if (serverSocket < 0) {
			std::cout << "Client Socket Error!"; ExitProcess(EXIT_FAILURE);
		}
	}
	// DEFINE TARGET EXPECTED SERVER PROTOCOL SETTINGS
	void defineServer(ServerInfo Server)
	{
		ZeroMemory(&serverdef, sizeof(serverdef));
		serverdef.sin_family = AF_INET;
		serverdef.sin_port = htons(Server.port);
		serverdef.sin_addr.S_un.S_addr = inet_addr(Server.ip.c_str());
	}
	// CONNECT TO SERVER
	int connectServer(ServerInfo Server)
	{
		while (connect(Server.socket, (sockaddr*)&serverdef, sizeof(serverdef)) != 0) 
		{
			if (WSAGetLastError() == 10061)
			{
				std::cout << "Unable to connect to server, retrying...\n";
				sleep_for(2s);
				return -1;
			}
			else if (WSAGetLastError() != 10061)
			{
				std::cout << "Connect Error! " << WSAGetLastError() << "\n\n"; ExitProcess(EXIT_FAILURE);
			}
		}
		return 1;
	}
	// RECEIVE DATA FROM SERVER
	std::string receiveData(ServerInfo Server)
	{
		char buf[512] = {};
		int connectStatus = 1;
		int bytefullcount = 0;
		while (true)
		{
			int byteCount = 0;
			byteCount = recv(Server.socket, buf, 512, 0);

			// ERROR HANDLING
			if (byteCount == SOCKET_ERROR) 
			{ 
				if (WSAGetLastError() == 10054)
				{
					std::cout << "\n\nLost Connection to Server! " << WSAGetLastError();
					connectStatus = 0;
					break;
				}
				if (WSAGetLastError() == 0)
					std::cout << "\n\nLost Connection to Server! " << WSAGetLastError();
				connectStatus = 0;
				break;
			}
			if (byteCount == 0) { std::cout << "\n\nLost Connection to Server! " << WSAGetLastError(); connectStatus = 0; break; }
			else bytefullcount += byteCount;

			std::string finalMessage = buf;
			ZeroMemory(buf, sizeof(buf));

			if (bytefullcount > 0)
			{
				return finalMessage;
			}
		}
		if (connectStatus == 0)
		{
			std::string final = "CONN_END";
			return final;
		}
		return "NULL";
	}
	// PRINT TO CONSOLE
	void printData(std::string finalMessage) 
	{
		std::cout << finalMessage;
	}
	// SEND INPUT TO SERVER
	void sendInput(ServerInfo Server)
	{
		std::string input;
		std::getline(std::cin, input);
		sendData(Server, input);
	}
	// SEND DATA TO SERVER
	void sendData(ServerInfo Server, std::string finalMessage)
	{
		char sendbuf[512];
		ZeroMemory(sendbuf, sizeof(sendbuf));
		strcpy_s(sendbuf, sizeof(sendbuf), finalMessage.c_str());
		send(Server.socket, sendbuf, strlen(sendbuf), 0);
	}
	// PING TARGETS
	void ping()
	{
		std::string tosend = "ABCDEFG";
		DWORD ReplySize = sizeof(ICMP_ECHO_REPLY) + sizeof(tosend);
		LPVOID ReplyBuffer = (VOID*) malloc(ReplySize);
		HANDLE hIcmpFile;

		hIcmpFile = IcmpCreateFile();
		if (hIcmpFile == INVALID_HANDLE_VALUE) {
			printf("\tUnable to open handle.\n");
			printf("IcmpCreatefile returned error: %ld\n", GetLastError());
		}

		for (auto ip : ips)
		{
			//inet_addr(ip.c_str())
			IcmpSendEcho(hIcmpFile, inet_addr(ip.c_str()), &tosend, 9, NULL, ReplyBuffer, ReplySize, 4000);
			PICMP_ECHO_REPLY pEchoReply = (PICMP_ECHO_REPLY)ReplyBuffer;
			unsigned long rtt = pEchoReply->RoundTripTime;
			PingInfo result(ip, rtt);
			results.push_back(result);
		}
		//for (auto result : results)
		//{
		//	int pos = 0;
		//	if (result.rtt == 0)
		//	{
		//		results.erase(results.begin() + pos);
		//	}
		//	else pos++;
		//}

		IcmpCloseHandle(hIcmpFile);
	}
	// INIT AND START CONNECT
	ServerInfo start(ServerInfo Server)
	{
		int connectStatus = -1;
		init();
		createClientSocket();
		Server.setSocket(serverSocket);
		defineServer(Server);
		
		while (connectStatus == -1)
		{
			connectStatus = connectServer(Server);
		}

		return Server;
	}
};



//RESOLVE FUNCTION (for failsafes)
